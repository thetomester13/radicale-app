#!/usr/bin/env node

'use strict';

/* global describe */
/* global after */
/* global before */
/* global it */

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    url = require('url'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

var VDIRSYNCER = process.env.VDIRSYNCER || 'vdirsyncer';

console.log('===============================================');
console.log(' This test requires vdirsyncer to be installed (using ' + VDIRSYNCER + ' )');
console.log('===============================================');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    var collectionId;
    var TEST_TIMEOUT = 10000;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var collectionName = 'mycollection';

    var browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
   }

    function login(callback) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/.web/');
        }).then(function () {
            return waitForElement(By.xpath('//input[@data-name="user"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return waitForElement(By.xpath('//input[@data-name="user"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//*[@id="logoutview"]/*[1]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//input[@data-name="user"]'));
        }).then(function () {
            callback();
        });
    }

    function createCollection(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return waitForElement(By.xpath('//input[@data-name="user"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            return waitForElement(By.xpath('//a[@data-name="new"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[@data-name="new"]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//section[@id="createcollectionscene"]/*/input[@data-name="displayname"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/input[@data-name="displayname"]')).sendKeys(collectionName);
        }).then(function () {
            return browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/select[@data-name="type"]/option[1]')).click();   // select addressbook
        }).then(function () {
            return browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/button[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//span[text()="' + collectionName + '"]'));
        }).then(function () {
            callback();
        });
    }

    function getCollection(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return waitForElement(By.xpath('//input[@data-name="user"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//section[@id="collectionsscene"]/article[1]//a[1]')).getAttribute('href');
        }).then(function (href) {
            var tmp = url.parse(href);
            collectionId = tmp.path.split('/').filter(function (f) { return f; }).pop();

            console.log('Using collection: ', collectionId);

            callback();
        });
    }

    function syncAddressBook() {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));
        fs.writeFileSync(addressBookPath + 'test_contact.vcf', fs.readFileSync(testContactFile));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        execSync(VDIRSYNCER + ' discover ', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        expect(fs.readFileSync(testContactFile, 'utf-8')).to.equal(fs.readFileSync(addressBookPath + 'test_contact.vcf', 'utf-8'));
    }

    function checkAddressBook() {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        console.log('VDIRSYNCER_CONFIG=/tmp/vdirsyncer/config vdirsyncer sync');
        execSync(VDIRSYNCER + ' discover test_contacts', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        var contactRemoteFileName = fs.readdirSync(addressBookPath)[0];
        expect(contactRemoteFileName).to.be.a('string');

        var testContactFileContent = fs.readFileSync(testContactFile, 'utf-8');
        var testContactRemoteFileContent = fs.readFileSync(addressBookPath + '/' + contactRemoteFileName, 'utf-8');

        // remove the dynamic radicale id
        testContactRemoteFileContent = testContactRemoteFileContent.replace('X-RADICALE-NAME:' + contactRemoteFileName + '\n', '');

        expect(testContactFileContent).to.equal(testContactRemoteFileContent);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create collection', createCollection);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('can sync addressbook', syncAddressBook);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);

            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');

            done();
        });
    });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id org.radicale.cloudronapp2 --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create collection', createCollection);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('can sync addressbook', syncAddressBook);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('addressbook is still ok', checkAddressBook);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
