#!/bin/bash

set -eu -o pipefail

echo "=========================="
echo "     Radicale startup     "
echo "=========================="

echo "=> Update radicale config"
sed -e "s/ldap_url = ldap:\/\/.*/ldap_url = ldap:\/\/${CLOUDRON_LDAP_SERVER}:${CLOUDRON_LDAP_PORT}/" \
    -e "s/ldap_base = .*/ldap_base = ${CLOUDRON_LDAP_USERS_BASE_DN}/" \
    -e "s/ldap_binddn = .*/ldap_binddn = ${CLOUDRON_LDAP_BIND_DN}/" \
    -e "s/ldap_password = .*/ldap_password = ${CLOUDRON_LDAP_BIND_PASSWORD}/" \
    "/app/code/config" > "/run/config"

# Remove with next release
echo "=> Cleanup old rights file"
rm -rf /app/data/rights

echo "=> Ensure folder permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start radicale"
exec /usr/local/bin/gosu cloudron:cloudron radicale --config /run/config
