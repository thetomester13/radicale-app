FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y python3-setuptools python3-wheel && rm -rf /var/cache/apt /var/lib/apt/lists

ARG VERSION=3.0.6

RUN python3 -m pip install --upgrade radicale==${VERSION}

ADD radicale_cloudron_ldap_auth /app/code/radicale_cloudron_ldap_auth
RUN cd /app/code/radicale_cloudron_ldap_auth && python3 -m pip install .

ADD config start.sh /app/code/

CMD [ "/app/code/start.sh" ]
