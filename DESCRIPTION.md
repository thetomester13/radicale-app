This app packages Radicale version <upstream>3.0.6</upstream>

### The Radicale Project is a CalDAV (calendar) and CardDAV (contact) server solution.

Calendars and address books can be viewed, edited and synced by calendar and contact clients on mobile phones or computers.

Officially supported clients are listed [here](https://radicale.org/2.1.html#documentation/clients), other DAV compatible clients may work as well.

